import { Row, Col } from "react-bootstrap";

import { Link } from "react-router-dom";
import Banner from "../components/Banner";

export default function Error({showErrorBanner}) {

  return <Banner showErrorBanner={showErrorBanner}/>;
}

{
  /* <Row>
      <Col className="p-5">
        <h1>Page Not Found</h1>
        <p>Go back to the <Link to="/" style={{textDecoration:"none"}}>homepage</Link></p>
      </Col>
    </Row> */
}
