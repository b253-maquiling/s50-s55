import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";

import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "Owen Orange";
// const user = {
//   firstName: "Lulu",
//   lastName: "Tamayo"
// }

// function fortmatName(user){
//   return user.firstName + " " + user.lastName
// }

// const element = <h1>Hello, {fortmatName(user)}</h1>;

// const root = ReactDOM.createRoot(document.querySelector("#root"));

// root.render(element);
