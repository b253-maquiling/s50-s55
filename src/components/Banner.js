import { useState } from "react";
import { Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function Banner({ showErrorBanner }) {

  const [show, setShow] = useState(showErrorBanner)

  console.log(showErrorBanner);
  if (show) {
    return (
      <Row>
        <Col className="p-5">
          <h1>Page Not Found</h1>
          <p>
            Go back to the{" "}
            <Link to="/" style={{ textDecoration: "none" }} onClick={()=> setShow(false)}>
              homepage
            </Link>
          </p>
        </Col>
      </Row>
    );
  }
  return (
    <Row>
      <Col className="p-5">
        <h1>Zuitt Coding Bootcamp</h1>
        <p>Opportunities for everyone, everywhere</p>
        <Button variant="primary">Enroll Now!</Button>
      </Col>
    </Row>
  );
}
